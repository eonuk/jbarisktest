import os
import tempfile
import pandas as pd
from flask import render_template, request, jsonify
from . import app
from .risk.models.depths import Depths
from .risk.models.vulnerability_curve import VulnerabilityCurve
from .risk.risk_calculator import calculate_expected_damage

# absolute path to this file (for generating paths to data files within package)
data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')


"""
ROUTE -> INDEX
The main application form using default vulnerability data
"""
@app.route('/', methods=['GET', 'POST'])
def index():
    # render app page using the default vulnerability_curve.csv data
    pth = os.path.join(data_dir, 'vulnerability_curve.csv')
    vc = VulnerabilityCurve.init_with_file(pth)

    return render_template('index.html', vulnerability_curve_data=vc.convert_to_dict())


"""
ROUTE -> CALC EXPECTED DAMAGE
User has submitted request to calculate the expected damage. Parse request data, then calc & return expected_damage
"""
@app.route('/calc_expected_damage', methods=['POST'])
def calc_expected_damage():
    # create depths model. Use the user's uploaded file. If not provided, use the default file included in package
    depths = _create_depth_model(request.form['inundation_percentage'], request.files)

    # create the vulnerability-curve model using values within the web form
    vc = _create_vulnerability_model(request.form)

    # calculate expected damage
    expected_damage = calculate_expected_damage(depths, vc)

    return jsonify(expectedDamage=expected_damage)



# -----------------------------------------------------------------------------------------------------------------
# PRIVATE

def _create_depth_model(inundation_percentage, files):
    """
    CREATE DEPTH MODEL
    Factory creating depth model from form's posted depth file along with the inundation percentage
    If one is not provided, then use the inbuilt depths.csv file
    :param files: the request's file object
    :return: an instance of a depths model
    """
    if len(files) > 0:
        # save the uploaded depths file to a temp file
        file = files['file']
        pth = tempfile.NamedTemporaryFile().name
        file.save(pth)
    else:
        # if no file was supplied, use the default depths.csv file
        pth = os.path.join(data_dir, 'depths.csv')

    return Depths.init_with_file(float(inundation_percentage), pth)


def _create_vulnerability_model(form):
    """
    CREATE VULNERABILITY MODEL
    Factory creating vulnerability model from form's posted data
    :param form: the request's form object
    :return: a instance of vulnerability model

    TODO: Implement validation of values & continuity of depth ranges
    """
    df = pd.DataFrame(columns=['lower_boundary', 'upper_boundary', 'damage'])

    # continuously parse the data until no more available.
    try:
        i = 0
        while True:
            # add one complete row to the dataframe at a time...
            lb = float(form['lower_boundary_{}'.format(i)])
            ub = float(form['upper_boundary_{}'.format(i)])
            dmg = float(form['damage_{}'.format(i)])
            df.loc[i] = [lb, ub, dmg]
            i = i + 1
    except:
        # no more data
        pass

    return VulnerabilityCurve(df)
