from jbarisktest import app

# to run the service from cmdline: "python -m jbarisktest". Use pythonw instead to run as a background process
if __name__ == '__main__':
    app.run()