import pandas as pd

class Depths:
    """
    DEPTHS
    A data model for the depth data together with helper function(s).

    Dev Notes:
    1. The primary constructor takes a dataframe to help with unittests.
    """
    # properties
    inundation_percentage = None
    df = None


    def __init__(self, inundation_percentage, df):
        """
        INIT
        Constructor with a suitable dataframe.
        :param filename:
        """
        self.inundation_percentage = inundation_percentage
        self.df = df


    @classmethod
    def init_with_file(cls, inundation_percentage, filename):
        """
        INIT FROM FILE
        Constructor using a filename of a csv file
        :param filename:
        :return:
        """
        df = pd.read_csv(filename)
        return cls(inundation_percentage, df)


    def get_relative_frequency_table(self, bin_edges):
        """
        GET RELATIVE FREQUENCY TABLE
        Calculate a relative frequency table of the depth samples, given a sequence of bin edges

        :param bin_edges:
        :return:
        """
        # identify which bin each depth sample falls into
        self.df['bin'] = pd.cut(self.df['Depth (m)'], bin_edges)

        # calc a flooded frequency table by grouping by each bin and counting the number of samples in each
        ft = self.df.groupby('bin').count().rename(columns={'Depth (m)': 'frequency'})

        # calc a flooded relative frequency table by dividing each count by total number of samples.
        rft = ft / self.df.shape[0]
        rft = rft.rename(columns={'frequency': 'relative_frequency'})

        # adjust the rft to account for the non-inundated area
        rft = rft * self.inundation_percentage / 100

        return rft
