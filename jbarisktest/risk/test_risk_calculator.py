import unittest
import pandas as pd
from models.depths import Depths
from models.vulnerability_curve import VulnerabilityCurve
from risk_calculator import calculate_expected_damage


class TestRiskCalculator(unittest.TestCase):
    """
    TEST RISK CALCULATOR
    Some tests to ensure that the relative frequency table in the depth model and the expected damage value are working
    TODO: Separate tests for the depth and vulnerability models should be done as well as the risk-calculator
    """

    def setUp(self):
        self.depth_data = pd.DataFrame(data=[0.0001, 0.5, 0.9999, 1, 1.0001, 1.9999, 2, 2.0001, 2.5, 3.5],
                                       columns = ['Depth (m)'])
        self.vc_data = pd.DataFrame(data={'lower_boundary': [0, 1, 2, 3],
                                          'upper_boundary': [1, 2, 3, 4],
                                          'damage': [2000, 5000, 13000 , 19000]})


    def test_depth_rft_1(self):
        # test that the sum of the depth's relative frequency table adds up to 1.0
        inundation_percentage = 100
        dm = Depths(inundation_percentage, self.depth_data)
        bin_edges = [0,1,2,3,4]
        rft = dm.get_relative_frequency_table(bin_edges)
        sum = round(rft['relative_frequency'].sum(), 8)
        self.assertEqual(sum, 1.0)


    def test_depth_rft_2(self):
        # same test as above, but reduce the inundation percentage. check that sum equates to this total percentage
        inundation_percentage = 65
        dm = Depths(inundation_percentage, self.depth_data)
        bin_edges = [0,1,2,3,4]
        rft = dm.get_relative_frequency_table(bin_edges)
        sum = round(rft['relative_frequency'].sum(), 8)
        self.assertEqual(sum, 0.65)


    def test_risk_calculator_1(self):
        # test the expected damage, compare result with value calculated manually in spreadsheet
        inundation_percentage = 100
        dm = Depths(inundation_percentage, self.depth_data)
        vc = VulnerabilityCurve(self.vc_data)
        expected_damage = calculate_expected_damage(dm, vc)
        self.assertEqual(expected_damage, 6800)


    def test_risk_calculator_2(self):
        # same test as above, but reduce the inundation percentage. check value compares with manually calc-ed value
        inundation_percentage = 40
        dm = Depths(inundation_percentage, self.depth_data)
        vc = VulnerabilityCurve(self.vc_data)
        expected_damage = calculate_expected_damage(dm, vc)
        self.assertEqual(expected_damage, 2720)


if __name__ == '__main__':
    unittest.main()