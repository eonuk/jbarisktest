
def calculate_expected_damage(depths, vc):
    """
    CALCULATE EXPECTED DAMAGE
    For a given depths and vulnerability models, calculate the 'expected damage' to an asset that resides in an
    unknown location.

    Dev Notes:
    1. Dependency injection of the models ensures that this code can easily be tested.

    :param depths:  model featuring depth samples
    :param vc:      model featuring vulnerability curve
    :return:
    """
    # calc a relative frequency table of the depth samples using the depth-ranges specified in the vc
    bin_edges = vc.get_bin_edges()
    df = depths.get_relative_frequency_table(bin_edges)

    # create new 'Damage' column that specifies each depth range's associated damage value (taken from the vc)
    for row in df.itertuples():
        df.loc[row.Index, 'vc_damage'] = vc.get_damage_value(row.Index.left, row.Index.right)

    # calculate the expected value = the sum of each (relative-frequency * associated damage value)
    df['relative_damage'] = df['relative_frequency'] * df['vc_damage']
    expected_damage = df['relative_damage'].sum()

    return expected_damage

