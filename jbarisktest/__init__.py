from flask import Flask

application = app = Flask(__name__)

# import Flask's routes (views) - at bottom of the file to avoid circular imports (standard practice)
from jbarisktest import routes