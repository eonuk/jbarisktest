$(function() {
    $('button').click(function() {
        $.ajax({
            url: '/calc_expected_damage',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
                span = document.getElementById("expectedDamage");
                span.textContent = "Expected Damage = £" + response.expectedDamage.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');;
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});