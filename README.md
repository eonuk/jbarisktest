# JBA Risk Test

## Task Overview
The JBA Risk Test is a web-application that calculates the expected damage for a risk in a flood event, given the Depth sample data; Percentage of area that is flooded; and the Vulnerability Curve.

#### Solution
The expected damaged is calculated as per "expected value" (μ - mu) via a frequency table. A count is made of the number of depth samples that fall in each depth range that is defined in the vulnerability curve. The table is converted to a relative frequency table to identify the proportion of samples within each depth range. Care must be taken as only 75% of the area is actually flooded, so these values are adjusted accordingly. The expected damage is then calculated as the sum of each relative frequency * its associated damage value.

This was briefly tested in spreadsheet and via Jupyter with simple data to ensure the solution made sense , and to script the relevant Pandas based operations. The logic was then implemented using PyCharm, refactored and then utilised within a Flask web application.

- - -

## Prerequisites / Installation
* Python (3.6.4)
* Developed and tested on Macbook Pro using Chrome & Opera


#### Installation
Create a suitable Python3 virtual environment and then install the distribution package:

    pip install jbarisktest-0.1.1.zip


#### Starting the Service
Activate the virtual environment and then run the JBA-Risk-Test service:

    python -m jbarisktest


#### Viewing in Web Browser
Launch the localhost address that is shown when the service is started into a web browser:

    http://127.0.0.1:5000/

- - -

# Development Notes
The application has been built using Python 3 with the following dependancies:
* Pandas
* Flask

The main logic of the application resides within the "risk" python package. Two data models encapsulate the data together with helper methods. Care has been taken to ensure that they rely on dependency injection to aid re-use and unit testing. Then, the module "risk_calculator" takes instances of these models and returns the expected damage. Again, dependencies are injected to make testing possible.

This logic is used by a simple Flask single-page web application. A template is used to render the main page, which is populated with data taken from the vulnerability curve model to display the revelant number of depth ranges fields. Once the request is sent to the Flask app, the result is returned and handled by AJAX (using JQuery) to display the result directly without having to reload the page.

#### Testing
A small amount of unit tests reside in "test_risk_calculator". These compare the results calculated against some test data with values that I manually calculated in a spreadsheet using the same data. For the scope of this task, I have not fully implemented a comprehensive set of unit tests, but this could easily be done for the two models as well as the risk calculator

#### Further development list
If development were to continue, I would prioritize the following:
1. Validation of the form data sent via the user's post request to avoid accidential errors by the user.
2. More unit tests.
3. Re-develop the html template and CSS with better design.


