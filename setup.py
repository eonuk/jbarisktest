from setuptools import setup
import os

# install-requires will be set to values in requiremente.txt. This ensures exact versions of packages are installed
# http://stackoverflow.com/questions/14399534/how-can-i-reference-requirements-txt-for-the-install-requires-kwarg-in-setuptool
with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name="jbarisktest",
    version="0.1.1",
    author="Ian Webb",
    author_email="ian.webb71@outlook.com",
    description=("A test application to calculate expected damage given depth & vulnerability curve data."),
    packages=['jbarisktest',
              'jbarisktest.risk',
              'jbarisktest.risk.models',
              ],
    include_package_data=True,
    install_requires=required,
)